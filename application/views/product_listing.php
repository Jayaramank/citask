<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>CI Task</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
  <style>
    p.card-text {
		min-height: 65px;
	}
  </style>
</head>
<body>
<div class="container">
  <h2>Products</h2>
  <p id="payment_response"></p>
    <div class="row">  
	<?php if($products) {  
	   foreach($products as $product) {
	?>
        <div class="col-3">
		  <div class="card">
			  <img class="card-img-top h-50" src="https://www.freeiconspng.com/uploads/no-image-icon-6.png" alt="<?php echo $product->name; ?>" title="<?php echo $product->name; ?>">
			  <div class="card-body">
				<h5 class="card-title">$<?php echo $product->price; ?></h5>
				<p class="card-text"><?php echo $product->name; ?></p>
				<button class="btn btn-primary btn-block" onclick="pay(<?php echo $product->price; ?>,'<?php echo $product->name; ?>')">Pay Now</button>
			  </div>
			</div>
		</div>	  
	<?php } 
	} ?>      
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<script src="https://checkout.stripe.com/checkout.js"></script>
<script type="text/javascript">
function pay(amount,name) {
var handler = StripeCheckout.configure({
	key: "<?php echo $this->config->item('stripe_api_key'); ?>", // your publisher key id
	locale: 'auto',
	token: function (token) {
    console.log(token);		
	$.ajax({
		url:"<?php echo base_url('index.php/welcome/payment'); ?>",
		method: 'post',
		data: { tokenId: token.id, amount: amount*100,itemname:name,},
		dataType: "json",
		success: function( response ) {
		    console.log(response.data);
			if(response.success ==1){
			   $('#payment_response').html('<span style="color:green">Charge created successfully !,Your charge ID:#.'+response.data.id+ ' and amount charged:$'+(response.data.amount/100).toFixed(2) +'</span>');
			} else {
			   $('#payment_response').html('<span style="color:red">.'+response.error.message+'</span>');
			}
			setTimeout(function(){ $('#payment_response').html(''); },3000);
		}
	})
}
});
handler.open({
	name: 'Demo Site',
	description: name,
	amount: amount * 100,
	allowRememberMe: false,
	email: "jayaramantest@yopmail.com"
});
}
</script>
</body>
</html>
