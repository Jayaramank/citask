<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{   
	    $this->load->model('product_model');
	    $data['products'] = $this->product_model->getProducts();
		$this->load->view('product_listing',$data);
	}
	public function payment(){
	  $return_data = []; 
      require_once(APPPATH.'libraries/stripe/init.php');     
      $stripeSecret = $this->config->item('stripe_secret_key'); //'sk_test_j5k0976GOLSOtiRzbDLpKqat00og5iM3cY'; 
      \Stripe\Stripe::setApiKey($stripeSecret); 
      try {	  
        $stripe = \Stripe\Charge::create ([
                "amount" => $this->input->post('amount'),
                "currency" => "usd",
                "source" => $this->input->post('tokenId'),
                "description" => $this->input->post('itemname')
        ]);
		$return_data = array('success' =>1, 'data'=>$stripe);
	  } catch (Exception $e) {
		 $return_data = array('success' =>0, 'data'=>$e->getMessage());
	  }		
      echo json_encode($return_data);
    }
}
